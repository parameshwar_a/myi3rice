# DarkSym

- DarkSym is a dark theme with symmetrical polybar configs
- Has Rofi for application launcher
- Using Xorg display server
- i3 WM 
- Requried Fonts:
	- [Zyzol](https://www.fontspace.com/zyzol-font-f67557)
	- [FiraCode](https://github.com/tonsky/FiraCode/releases), 
	- [FontAwesome](https://github.com/FortAwesome/Font-Awesome/releases)
	- Note: This I3 config contains ALT as the mod key
